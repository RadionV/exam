package com.project.exam.dto.teacher;

import com.project.exam.entity.Cathedra;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Getter
@Setter
public class ResponseTeacherDto {
    private String fio;
    private String cathedra;
}
