package com.project.exam.dto.teacher;

import com.project.exam.entity.Cathedra;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Getter
@Setter
public class CreateNewTeacherDto {

    private String fio;
    private String cathedra;

}
