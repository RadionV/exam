package com.project.exam.dto.teacher;

import lombok.Getter;
import lombok.Setter;

// TODO: 09.04.2021 bean validation
@Getter
@Setter
public class UpdateTeacherRequest {

    private Integer id;
    private String fio;
    private String cathedra;

}
