package com.project.exam.dto.student;

import lombok.Getter;
import lombok.Setter;

import java.security.Principal;

@Getter
@Setter
public class CreateStudentRequestDto {
    private String fio;
    private String speciality;
    private Integer course;
}
