package com.project.exam.dto.student;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateStudentRequest {
    private Integer id;
    private String fio;
    private String speciality;
    private Integer course;
}
