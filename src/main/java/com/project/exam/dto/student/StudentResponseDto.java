package com.project.exam.dto.student;

import com.project.exam.entity.Speciality;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentResponseDto {
    private String fio;
    private String speciality;
    private Integer course;
}
