package com.project.exam.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Cathedra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Version
    private Integer version;

    private String name;

    @OneToMany(mappedBy = "cathedra", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Teacher> teacherList;
}
