package com.project.exam.repository;

import com.project.exam.entity.Cathedra;
import com.project.exam.entity.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {
    Optional<Speciality> findByName(String name);
}
