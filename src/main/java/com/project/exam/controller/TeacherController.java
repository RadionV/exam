package com.project.exam.controller;

import com.project.exam.dto.student.StudentResponseDto;
import com.project.exam.dto.teacher.CreateNewTeacherDto;
import com.project.exam.dto.teacher.ResponseTeacherDto;
import com.project.exam.dto.teacher.UpdateTeacherRequest;
import com.project.exam.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/api/teacher/{id}")
    public ResponseTeacherDto getTeacherById(@PathVariable(name = "id") Integer id){
        return teacherService.getTeacherById(id);
    }

    @GetMapping("/api/teacher/all")
    public List<ResponseTeacherDto> getAllTeachers(){
        return teacherService.getAllTeacher();
    }

    @PostMapping("/api/teacher")
    public String createNewTeacher(@RequestBody CreateNewTeacherDto createNewTeacherDto){
        teacherService.createNewTeacher(createNewTeacherDto);
        return "ok";
    }

    @PutMapping("/api/teacher/")
    public String updateTeacher(@RequestBody UpdateTeacherRequest updateTeacherRequest){
        // fix put in main project
        return teacherService.updateTeacher(updateTeacherRequest);
    }

    @DeleteMapping("/api/teacher/{id}")
    public String deleteTeacher(@PathVariable(name = "id") Integer id){
        return teacherService.deleteTeacher(id);
    }

    @GetMapping("/api/teacher/{id}/students")
    public List<StudentResponseDto> findAllStudentsByTeacher(@PathVariable(name = "id") Integer id){
        return teacherService.findAllStudentsByTeacher(id);
    }



}
