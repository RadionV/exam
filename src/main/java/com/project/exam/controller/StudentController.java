package com.project.exam.controller;

import com.project.exam.dto.student.CreateStudentRequestDto;
import com.project.exam.dto.student.StudentResponseDto;
import com.project.exam.dto.student.UpdateStudentRequest;
import com.project.exam.dto.teacher.ResponseTeacherDto;
import com.project.exam.dto.teacher.UpdateTeacherRequest;
import com.project.exam.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/api/student/{id}")
    public StudentResponseDto getStudentById(@PathVariable(name = "id") Integer id){
        return studentService.getStudentById(id);
    }

    @GetMapping("/api/student/all")
    public List<StudentResponseDto> getAllStudents(){
        return studentService.getAllStudent();
    }

    @PostMapping("/api/student")
    public String createNewStudent(@RequestBody CreateStudentRequestDto createStudentRequestDto){
        studentService.createNewStudent(createStudentRequestDto);
        return "ok";
    }

    @PutMapping("/api/student/")
    public String updateStudent(@RequestBody UpdateStudentRequest updateStudentRequest){
        // fix put in main project
        return studentService.updateStudent(updateStudentRequest);
    }

    @DeleteMapping("/api/student/{id}")
    public String deleteStudent(@PathVariable(name = "id") Integer id){
        return studentService.deleteStudent(id);
    }

}
