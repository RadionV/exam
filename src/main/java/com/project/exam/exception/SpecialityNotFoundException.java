package com.project.exam.exception;

public class SpecialityNotFoundException extends RuntimeException {
    public SpecialityNotFoundException(String message) {
        super("Speciality " + message + " not found");
    }
}
