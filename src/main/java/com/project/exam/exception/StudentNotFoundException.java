package com.project.exam.exception;

import com.project.exam.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public class StudentNotFoundException extends RuntimeException{
    public StudentNotFoundException(Integer s) {
        super("Student with id " + s + " found" );
    }
}
