package com.project.exam.exception;

public class TeacherNotFoundException extends RuntimeException{
    public TeacherNotFoundException(Integer id) {
        super("Teacher with id " + id + " not found");
    }
}
