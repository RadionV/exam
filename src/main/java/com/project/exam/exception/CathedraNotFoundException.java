package com.project.exam.exception;

public class CathedraNotFoundException extends RuntimeException {
    public CathedraNotFoundException(String name) {
        super("Cathedra " + name + "not found");
    }
}
