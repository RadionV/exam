package com.project.exam.service;

import com.project.exam.dto.student.StudentResponseDto;
import com.project.exam.dto.teacher.CreateNewTeacherDto;
import com.project.exam.dto.teacher.ResponseTeacherDto;
import com.project.exam.dto.teacher.UpdateTeacherRequest;
import com.project.exam.entity.Cathedra;
import com.project.exam.entity.Student;
import com.project.exam.entity.Teacher;
import com.project.exam.exception.CathedraNotFoundException;
import com.project.exam.exception.TeacherNotFoundException;
import com.project.exam.repository.CathedraRepository;
import com.project.exam.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TeacherService {

    private final TeacherRepository teacherRepository;
    private final CathedraRepository cathedraRepository;


    public ResponseTeacherDto getTeacherById(Integer id){
        Teacher teacher= teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
        ResponseTeacherDto dto = new ResponseTeacherDto();
        dto.setFio(teacher.getFio());
        dto.setCathedra(teacher.getCathedra().getName());
        return dto;
    }

    public Integer createNewTeacher(CreateNewTeacherDto createNewTeacherDto){
        Cathedra cathedra = cathedraRepository.findByName(createNewTeacherDto.getCathedra()).
                orElseThrow(() -> new CathedraNotFoundException(createNewTeacherDto.getCathedra()));

        Teacher teacher = new Teacher();
        teacher.setFio(createNewTeacherDto.getFio());
        teacher.setCathedra(cathedra);

        Teacher save = teacherRepository.save(teacher);
        return save.getId();
    }

    // TODO: 09.04.2021 add pagination
    public List<ResponseTeacherDto> getAllTeacher(){
        List<Teacher> all = teacherRepository.findAll();
        List<ResponseTeacherDto> responseTeacherDtos = new ArrayList<>();

        for(Teacher teacher : all){
            // TODO: 09.04.2021 add mapper?
            ResponseTeacherDto responseTeacherDto = new ResponseTeacherDto();
            responseTeacherDto.setFio(teacher.getFio());
            responseTeacherDto.setCathedra(teacher.getCathedra().getName());
            responseTeacherDtos.add(responseTeacherDto);
        }
        return responseTeacherDtos;
    }

    public String updateTeacher(UpdateTeacherRequest updateTeacherRequest){
        Teacher teacher = teacherRepository.findById(updateTeacherRequest.getId()).orElseThrow(() -> new TeacherNotFoundException(updateTeacherRequest.getId()));

        if (!updateTeacherRequest.getFio().isEmpty()){
            teacher.setFio(updateTeacherRequest.getFio());
        }

        if (!updateTeacherRequest.getCathedra().isEmpty()){
            Cathedra cathedra = cathedraRepository.findByName(updateTeacherRequest.getCathedra()).
                    orElseThrow(() -> new CathedraNotFoundException(updateTeacherRequest.getCathedra()));
            teacher.setCathedra(cathedra);
        }

        teacherRepository.save(teacher);
        return "ok";
    }

    public String deleteTeacher(Integer id){
        Teacher teacher = teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
        teacherRepository.delete(teacher);
        return "ok";
    }

    public List<StudentResponseDto> findAllStudentsByTeacher(Integer id){
        Teacher teacher = teacherRepository.findById(id)
                .orElseThrow(() -> new TeacherNotFoundException(id));

        List<StudentResponseDto> studentResponseDtos = new ArrayList<>();

        for (Student student : teacher.getStudentList()){
            StudentResponseDto studentResponseDto = new StudentResponseDto();
            studentResponseDto.setCourse(student.getCourse());
            studentResponseDto.setFio(student.getFio());
            studentResponseDto.setSpeciality(student.getSpeciality().getName());
            studentResponseDtos.add(studentResponseDto);
        }
        return studentResponseDtos;
    }
}
