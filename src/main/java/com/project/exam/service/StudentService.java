package com.project.exam.service;

import com.project.exam.dto.student.CreateStudentRequestDto;
import com.project.exam.dto.student.StudentResponseDto;
import com.project.exam.dto.student.UpdateStudentRequest;
import com.project.exam.dto.teacher.ResponseTeacherDto;
import com.project.exam.dto.teacher.UpdateTeacherRequest;
import com.project.exam.entity.Cathedra;
import com.project.exam.entity.Speciality;
import com.project.exam.entity.Student;
import com.project.exam.entity.Teacher;
import com.project.exam.exception.CathedraNotFoundException;
import com.project.exam.exception.SpecialityNotFoundException;
import com.project.exam.exception.StudentNotFoundException;
import com.project.exam.exception.TeacherNotFoundException;
import com.project.exam.repository.SpecialityRepository;
import com.project.exam.repository.StudentRepository;
import com.project.exam.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final SpecialityRepository specialityRepository;
    private final TeacherRepository teacherRepository;

    public StudentResponseDto getStudentById(Integer id){
        Student student= studentRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));

        StudentResponseDto studentResponseDto = new StudentResponseDto();
        studentResponseDto.setCourse(student.getCourse());
        studentResponseDto.setFio(student.getFio());
        studentResponseDto.setSpeciality(student.getSpeciality().getName());

        return studentResponseDto;
    }


    public Integer createNewStudent(CreateStudentRequestDto createStudentRequestDto){

        Speciality speciality = specialityRepository.findByName(createStudentRequestDto.getSpeciality())
                .orElseThrow(() -> new SpecialityNotFoundException(createStudentRequestDto.getSpeciality()));

        Student student = new Student();
        student.setFio(createStudentRequestDto.getFio());
        student.setCourse(createStudentRequestDto.getCourse());
        student.setSpeciality(speciality);

        Student persistStudent = studentRepository.save(student);
        return persistStudent.getId();
    }

    // TODO: 09.04.2021 add pagination
    public List<StudentResponseDto> getAllStudent(){
        List<Student> all = studentRepository.findAll();
        List<StudentResponseDto> studentResponseDtos = new ArrayList<>();

        for(Student student : all){
            // TODO: 09.04.2021 add mapper?
            StudentResponseDto studentResponseDto = new StudentResponseDto();
            studentResponseDto.setFio(student.getFio());
            studentResponseDto.setCourse(student.getCourse());
            studentResponseDto.setSpeciality(student.getSpeciality().getName());
            studentResponseDtos.add(studentResponseDto);
        }
        return studentResponseDtos;
    }

    public String updateStudent(UpdateStudentRequest updateStudentRequest){
        Student student = studentRepository.findById(updateStudentRequest.getId())
                .orElseThrow(() -> new StudentNotFoundException(updateStudentRequest.getId()));

        if (!updateStudentRequest.getFio().isEmpty()){
            student.setFio(updateStudentRequest.getFio());
        }

        if (updateStudentRequest.getCourse() != null){
            student.setCourse(updateStudentRequest.getCourse());
        }

        if (!updateStudentRequest.getSpeciality().isEmpty()){
            Speciality speciality = specialityRepository.findByName(updateStudentRequest.getSpeciality()).
                    orElseThrow(() -> new SpecialityNotFoundException(updateStudentRequest.getSpeciality()));
            student.setSpeciality(speciality);
        }

        studentRepository.save(student);
        return "ok";
    }

    public String deleteStudent(Integer id){
        Student student = studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
        studentRepository.delete(student);
        return "ok";
    }

    public List<ResponseTeacherDto> findAllTeachersByStudent(Integer id){
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException(id));

        List<ResponseTeacherDto> responseTeacherDtos = new ArrayList<>();

        for (Teacher teacher : student.getTeacherList()){
            ResponseTeacherDto teacherDto = new ResponseTeacherDto();
            teacherDto.setFio(teacher.getFio());
            teacherDto.setCathedra(teacher.getCathedra().getName());
            responseTeacherDtos.add(teacherDto);
        }
        return responseTeacherDtos;
    }

//    public void appendStudentToTeacher(){
//        Student student = studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
//
//    }

}
